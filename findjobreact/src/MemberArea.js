

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import logo from './dlogo.png';
import RegisterEvent from "./RegisterEvent";
import ListMyJob from "./ListMyJob";
import MyProfile from "./MyProfile";


function MemberArea(props) {

	return ( 
		<div>
		<div>
        <img src={logo} alt="cur" class="centers"
          height={100}
          width={150}
        />
      </div>
	  <br></br>
	     
	   <div className = "titlea titlea-color"><div className = "texta">HomePage</div></div>
	   <h2>Welcome to CESJob </h2>
	   <h2>Hello</h2>
		<h3> <Link to="/MemberArea/RegisterEvent"> <button type="submit" className="btn btn-primary btn-block">
		Apply for a job</button></Link> 
			 <Link to="/MemberArea/ListMyJob"><button type="submit" className="btn btn-primary btn-block">List Job</button> </Link>  
			 <Link to="/MemberArea/MyProfile"><button type="submit" className="btn btn-primary btn-block">MyProfile</button> </Link>  
			 ล็อคเอาท์ 
		</h3>
		
		<hr />
		<Switch>
            
            <Route path="/MemberArea/RegisterEvent" component={RegisterEvent} />
            <Route path="/MemberArea/ListMyJob" component={ListMyJob} />
			<Route path="/MemberArea/MyProfile" component={MyProfile} />
            
          </Switch>
		
	
	</div>
	
	
	);

}

export default MemberArea;