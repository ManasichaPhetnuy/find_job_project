import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import logo from './dlogo.png';

import AdminAddJob from './AdminAddJob';
import AdminConfig from './AdminConfirmRegistration';


function AdminHome() {
	
	
	return (
	<div>
		<div>
        <img src={logo} alt="cur" class="centers"
          height={100}
          width={150}
        />
      </div>
	  <br></br>
	     
	   <div className = "titlea titlea-color"><div className = "texta">Admin HomePage</div></div>
	   <h2>Welcome CESJob Admin</h2>
		<h3> <Link to="/AdminHome/AdminAddJob"> <button type="submit" className="btn btn-primary btn-block">AddJob</button></Link> 
			 <Link to="/AdminHome/AdminConfirmRegistration"><button type="submit" className="btn btn-primary btn-block">ConfirmRegistration</button> </Link>  
			 ล็อคเอาท์ 
		</h3>
		
		<hr />
		
		<Switch>
            
            <Route path="/AdminHome/AdminAddJob" component={AdminAddJob} />
            <Route path="/AdminHome/AdminConfirmRegistration" component={AdminConfig} />
            
          </Switch>
		
	
	</div>)

}
export default AdminHome;