

import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	 Redirect
  } from "react-router-dom";
import logo from './dlogo.png';
import RegisterEvent from "./RegisterEvent";
import ListMyJob from "./ListMyJob";
import MyProfile from "./MyProfile";

import { useState, useEffect } from 'react';

function MemberArea(props) {
	const [hasToken, setHasToken] = useState(true);	

	
	
	function logout() {
		//ระบบ Logout ในระบบ Stateless Authentication นั้น ไม่จำเป็นต้องยิง REST API
		//เพียงเคลียร์ token ออกจาก Memory แล้ว redirect กลับหน้า Login เป็นอันเพียงพอ
		
		sessionStorage.clear();
		setHasToken(false);
		alert("Logged Out");
	}

	return ( 
		<div>
		<div>
        <img src={logo} alt="cur" class="centers"
          height={100}
          width={150}
        />
      </div>
	  <br></br>
	     
	   <div className = "titlea titlea-color"><div className = "texta">HomePage</div></div>
	   <h2>Welcome to CESJob </h2>
	   <hr />
		<h3> <Link to="/MemberArea/RegisterEvent"> <button type="submit" className="btn btn-primary btn-block">
		Apply for a job</button></Link> 
			 <Link to="/MemberArea/ListMyJob"><button type="submit" className="btn btn-primary btn-block">List Job</button> </Link>  
			 <Link to="/MemberArea/MyProfile"><button type="submit" className="btn btn-primary btn-block">MyProfile</button> </Link>  
			 <hr />
			 <button className = "brtn" onClick={()=>{ logout() }}> Logout </button>   
		</h3>
		
	
		<Switch>

		{!hasToken && <Redirect to="/" /> }
            
		<Route exact path="/"> 
			</Route>
            <Route path="/MemberArea/RegisterEvent" component={RegisterEvent} />
            <Route path="/MemberArea/ListMyJob" component={ListMyJob} />
			<Route path="/MemberArea/MyProfile" component={MyProfile} />
            
          </Switch>
		
	
	</div>
	
	
	);

}

export default MemberArea;