import {
	Link,
	Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
function AdminConfirmRegistration() {

	const [hasToken, setHasToken] = useState(true);	
	const [eventList, setEventList] = useState([]);
	
	
	function sendConfirm(id){
		axios.put('http://localhost/api/v2/admin_confirmjob',
				{
					'api_token': sessionStorage.getItem('admin_api_token'),
					'id' : id,
				}
			).then (
				res=> {			
					if (res.data == "CONFIRMED") {
							alert("Successful approval");
							axios.get('http://localhost/api/v2/admin_listregister?api_token='+sessionStorage.getItem('admin_api_token'),
							).then (
								res=> {	
								
										
										let item_list = [];
										for(let i = 0;i < res.data.length;i++){
											item_list[i] = [ res.data[i].Name, res.data[i].Surname, res.data[i].JobDescription  ,res.data[i].NameCompany, res.data[i].ID ]; 
										}
										
										setEventList(item_list);
										
								}
							
							);
							
					}else {
							alert ("Have problem Can't apply");
					}
				}
			);
	}
	
	useEffect (()=>{
		
		console.log(sessionStorage.getItem('admin_api_token'));
		if (sessionStorage.getItem('admin_api_token')==null) {
			setHasToken(false);
		}else {
			axios.get('http://localhost/api/v2/admin_listregister?api_token='+sessionStorage.getItem('admin_api_token'),
			).then (
				res=> {	
				
						
						let item_list = [];
						for(let i = 0;i < res.data.length;i++){
							item_list[i] = [  res.data[i].Name, res.data[i].Surname, res.data[i].JobDescription ,res.data[i].NameCompany , res.data[i].ID ]; 
						}
						
						setEventList(item_list);
						
				}
			
			);
			
			
		
		}
	}, []);
	
	return (<div>
				
				<h2> ConfirmRegistration </h2>

				{eventList.map(eventItem => (
					<h4> - Name : {eventItem[0]}    Surname : {eventItem[1]} 
					<br></br>
					 Description : {eventItem[2]} 
					 <br></br> 
					 Company : {eventItem[3]}  
					 <br></br> 
					 <br></br> 
							<button  className = "btg" onClick={()=>{  sendConfirm(eventItem[4]) } } >Comform</button> 
					</h4>
				))}
				
				
			</div>);
		
}

export default AdminConfirmRegistration;