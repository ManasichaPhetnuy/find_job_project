import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	Redirect
  } from "react-router-dom";
import logo from './dlogo.png';

import AdminAddJob from './AdminAddJob';
import AdminConfig from './AdminConfirmRegistration';

import { useState, useEffect } from 'react'; 

function AdminHome() {
	const [hasToken, setHasToken] = useState(true);	

	
	function logout() {
		//ระบบ Logout ในระบบ Stateless Authentication นั้น ไม่จำเป็นต้องยิง REST API
		//เพียงเคลียร์ token ออกจาก Memory แล้ว redirect กลับหน้า Login เป็นอันเพียงพอ
		
		sessionStorage.clear();
		setHasToken(false);
		alert("Logged Out");
	}
	return (
	<div>
		<div>
        <img src={logo} alt="cur" class="centers"
          height={100}
          width={150}
        />
      </div>
	  <br></br>
	     
	   <div className = "titlea titlea-color"><div className = "texta">Admin HomePage</div></div>
	   <h2>Welcome CESJob Admin</h2>
		<h3> <Link to="/AdminHome/AdminAddJob"> <button type="submit" className="btn btn-primary btn-block">Add Job</button></Link> 
			 <Link to="/AdminHome/AdminConfirmRegistration"><button type="submit" className="btn btn-primary btn-block">Confirm Registration</button> </Link>  
			
		</h3>
		<hr />
		<center><button className = "brtn" onClick={()=>{ logout() }}> Logout </button></center>
		{!hasToken && <Redirect to="/" /> }
		<Switch>
            
            <Route path="/AdminHome/AdminAddJob" component={AdminAddJob} />
            <Route path="/AdminHome/AdminConfirmRegistration" component={AdminConfig} />
            
          </Switch>
		
	
	</div>)

}
export default AdminHome;