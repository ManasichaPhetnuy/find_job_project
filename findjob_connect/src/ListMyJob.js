import {
	Link,
	Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
function ListMyEvent(){

	const [hasToken, setHasToken] = useState(true);	
	const [eventList, setEventList] = useState([]);
	useEffect (()=>{
		
		
		if (sessionStorage.getItem('user_api_token')==null) {
			setHasToken(false);
		}else {
			axios.get('http://localhost/api/v2/listjob_user?api_token='+sessionStorage.getItem('user_api_token'),
			).then (
				res=> {	
						let item_list = [];
						for(let i = 0;i < res.data.length;i++){
							item_list[i] = [ res.data[i].JobDescription, res.data[i].NameCompany, res.data[i].NumberSalary, res.data[i].EducationalBackground, res.data[i].ThePlace, res.data[i].ConfirmJob ]; 
						}
						
						setEventList(item_list);
						
				}
			
			);
			
			
		
		}
	}, []);

	return ( 
	<div>
	
		<h2>  The job you are applying </h2>
	
		{eventList.map(eventItem => (
				<h4>Desscription : {eventItem[0]} 
				<br></br>
				Company : {eventItem[1]}  
				<br></br>
				Salary : {eventItem[2]}
				<br></br>
				Education : {eventItem[3]} 
				<br></br>
				Place : {eventItem[4]}   
				<br></br>
				Confirmation status : {eventItem[5]}  
				
				<br></br>
				<br></br> </h4>
		  ))}
			
	</div>);
	
}

export default ListMyEvent;