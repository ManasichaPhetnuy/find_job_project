import {
    Link,
    Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';

function AdminAddJob() {

    const [hasToken, setHasToken] = useState(true);	
	
	
	const [Description, setDescrip]    = useState("");
	const [Company, setCompany] 	   = useState("");
	const [Salary,  setSalary]         = useState("");
    const [Educational ,  setEducate]  = useState("");
    const [Place,  setPlace] 	       = useState("");
	
	useEffect (()=>{
			
		if (sessionStorage.getItem('admin_api_token')==null) {
			setHasToken(false);
		}
	}, []);
	
	
	function sendAddEvent(){
		axios.post('http://localhost/api/v2/admin_addjob',
				{
					'api_token': sessionStorage.getItem('admin_api_token'),
					'job_description' :Description,
					'name_company' :Company,
					'number_salary' : Salary,
					'educational_background' : Educational,
                    'the_place'  : Place,
				}
			).then (
				res=> {	
					
					if (res.data.STATUS == "SUCCESS") {
							alert("Successfully Added");
					}else {
							alert ("Have problem can't added");
					}
				}
			);
	}
	
	return (<div>
				{!hasToken && <Redirect to="/" /> }
				<h2> Add New Job</h2>

				<div className="form-group">
                    <label>Job Desciption</label><br/>
                   < input type="text"  className="form-control" placeholder="description" value={Description} 	onChange={(e)=>setDescrip(e.target.value)}	/ >
                    
                </div>
				<div className="form-group">
                    <label>Name Company</label><br/>
                    < input type="text"  className="form-control" placeholder="company" value={Company} 	onChange={(e)=>setCompany(e.target.value)}	/ >
                </div>
				<div className="form-group">
                    <label>Salary</label><br/>
                    < input type="text"  className="form-control" placeholder="salary" value={Salary} 	onChange={(e)=>setSalary(e.target.value)}	/ >
                </div>
				<div className="form-group">
                    <label>EducationBackground</label><br/>
                    < input type="text"  className="form-control" placeholder="educationbackground" value={Educational} 	onChange={(e)=>setEducate(e.target.value)}	/ >
                </div>
				<div className="form-group">
                    <label>The Place</label><br/>
                    < input type="text"  className="form-control" placeholder="place" value={Place} 	onChange={(e)=>setPlace(e.target.value)}	/ >
                </div>
				<button  className="btgn" onClick={()=>{sendAddEvent()} }>Send</button>
				
               
						
				
			
			</div>);
		
}

export default AdminAddJob;