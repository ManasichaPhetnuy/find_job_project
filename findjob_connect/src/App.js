import './App.css';  
import  Regis from './Regis/Regis';
import  Login from './Login/Login';
import  AdminHome from './AdminHome';
import MemberArea from'./MemberArea';



import { BrowserRouter as Router, Switch, Route, } from "react-router-dom";

function App() {
  return  (<Router>
    <div className="App">
      

      <div className="auth-wrapper">
        <div className="auth-inner">
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path="/Regis" component={Regis} />
            <Route path="/MemberArea" component={MemberArea} />
            <Route path="/AdminHome" component={AdminHome} />
            
          </Switch>
        </div>
      </div>
    </div></Router>
  );
}
	  
    
	

export default App;