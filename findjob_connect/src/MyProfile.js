import {
	Link,
	Redirect
  } from "react-router-dom";
  
  import pro from './user.png';
  import { useState, useEffect } from 'react';
  import axios from 'axios';
function MyProfile(){
	
	const [name, 	setName] 	= useState("");
	const [surname, setSurname] = useState("");
    const [age, setAge]         = useState("");
    const [birthday, setBirthday] = useState("");
    const [educate, setEducate] = useState("");
    const [factory, setFactory] = useState("");
    const [branch, setBranch]   = useState("");
    const [grade, setGrade]     = useState("");
    const [phone, setPhone]     = useState("");
	const [email,  	setEmail] 	= useState("");
	

	const [hasToken, setHasToken] = useState(true);	
	
	useEffect (()=>{
		
		
		if (sessionStorage.getItem('user_api_token')==null) {
			setHasToken(false);
		}else {
			axios.get('http://localhost/api/v2/user_profile?api_token='+sessionStorage.getItem('user_api_token'),
			).then (
				res=> {	
					setName(res.data[0].Name);
					setSurname(res.data[0].Surname);
					setAge(res.data[0].Age);
					setBirthday(res.data[0].BirhtDay);
					setEducate(res.data[0].Educational_Background);
					setFactory(res.data[0].FinishFaculty);
					setBranch(res.data[0].FinishBranch);
					setGrade(res.data[0].GraduateUniversity)
					setEmail(res.data[0].Gmail);
					setPhone(res.data[0].PhoneNumber);
				}
			);
			
			
		
		}
	}, []);

	return ( 
	<div>
	{!hasToken && <Redirect to="/" /> }
		<h2> MyProfile </h2>
		<div>
        <img src={pro} alt="cur" class="centers"
          height={110}
          width={110}
        />
      </div>
		<h4 className = "myprofile"> Name : {name}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Surname : {surname} </h4>
		<h4 className = "myprofile"> Age : {age} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Birthday : {birthday} </h4>
		<h4 className = "myprofile"> Educational Background : {educate} </h4>
		<h4 className = "myprofile"> Finish Factory : {factory} </h4>
		<h4 className = "myprofile"> Finish Branch : {branch} </h4>
		<h4 className = "myprofile"> Graduate University : {grade} </h4>
		<h4 className = "myprofile"> Grade : {grade} </h4>
		<h4 className = "myprofile"> Phone : {phone} </h4>
		<h4 className = "myprofile"> Email : {email} </h4>
	
	</div>);
	
}

export default MyProfile;