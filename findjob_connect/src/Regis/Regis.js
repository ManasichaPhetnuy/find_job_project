
import { useState } from 'react';
import axios from 'axios';
import './sheet.css';

import {
    Redirect,
    Link
} from "react-router-dom";

function Regis() {

    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [age, setAge] = useState("");
    const [birthday, setBirthday] = useState("");
    const [educate, setEducate] = useState("");
    const [factory, setFactory] = useState("");
    const [branch, setBranch] = useState("");
    const [grade, setGrade] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [username, setUsername] = useState("");
    const [pw1, setPw1] = useState("");
    const [pw2, setPw2] = useState("");

    const [isSuccess, setIsSuccess] = useState(false);
    function sendRegis() {

        if (pw1 != pw2) {
            alert("Password does not match Please enter a new one.");
        } else {
            axios.post('http://localhost/api/v2/register_user',
                {
                    "name": name,
                    "surname": surname,
                    "age": age,
                    "birht_day":birthday,
                    "educationalbackground": educate,
                    "finish_faculty": factory,
                    "finish_branch": branch,
                    "graduate_university": grade,
                    "gmail": email,
                    "phone_number": phone,
                    "username": username,
                    "password": pw1,
                }
            ).then(
                res => {
                    if (res.data.STATUS == "SUCCESS") {
                        alert("Successfully Applied");
                        setIsSuccess(true)
                    } else {
                        alert("Have a problem Can't apply");
                        setIsSuccess(false)
                    }
                }
            );

        }

    }


    return (<div>
        <h1> Signup </h1>

                    <div className="form-group">
                    <label>Name</label><br/>
                   < input type="text"  className="form-control" placeholder="name" value={name} 	onChange={(e)=>setName(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Surname</label><br/>
                   < input type="text"  className="form-control" placeholder="surname" value={surname}	onChange={(e)=>setSurname(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Age</label><br/>
                   < input type="text"  className="form-control" placeholder="age" value={age} 	onChange={(e)=>setAge(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>BirthDay</label><br/>
                   < input type="text"  className="form-control" placeholder="dd/mm/yyyy" value={birthday} 	onChange={(e)=>setBirthday(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Educational Background</label><br/>
                   < input type="text"  className="form-control" placeholder="education" value={educate}	onChange={(e)=>setEducate(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Finish Factory</label><br/>
                   < input type="text"  className="form-control" placeholder="factory" value={factory}	onChange={(e)=>setFactory(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Finish Branch</label><br/>
                   < input type="text"  className="form-control" placeholder="branch" value={branch} 	onChange={(e)=>setBranch(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Graduate University</label><br/>
                   < input type="text"  className="form-control" placeholder="university" value={grade} 	onChange={(e)=>setGrade(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Email Address</label><br/>
                   < input type="email"  className="form-control" placeholder="email" value={email} 	onChange={(e)=>setEmail(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Phone</label><br/>
                   < input type="text"  className="form-control" placeholder="phonenumber" value={phone} 	onChange={(e)=>setPhone(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Username</label><br/>
                   < input type="text"  className="form-control" placeholder="username" value={username}	onChange={(e)=>setUsername(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Password</label><br/>
                   < input type="password"  className="form-control" placeholder="password" value={pw1} 	onChange={(e)=>setPw1(e.target.value)}	/ >
                    </div>
                    <div className="form-group">
                    <label>Comform Password</label><br/>
                   < input type="password"  className="form-control" placeholder="password" value={pw2}	onChange={(e)=>setPw2(e.target.value)}	/ >
                    </div>
                    
            

        <button className = "btn btn-primary"onClick={() => sendRegis()}>Send</button>

        {isSuccess && <Redirect to="/" />}
        Already registered <Link to="/">Login</Link>
    </div>);
}

export default Regis;