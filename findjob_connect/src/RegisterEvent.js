import {
	Link,
	Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
function RegisterEvent(){
	const [hasToken, setHasToken] = useState(true);	
	const [eventList, setEventList] = useState([]);


	useEffect (()=>{
			
		if (sessionStorage.getItem('user_api_token')==null) {
			setHasToken(false);
		}
	}, []);
	
	
	function sendSignup(JobID){
		axios.post('http://localhost/api/v2/register_job',
				{
					'api_token': sessionStorage.getItem('user_api_token'),
					'job_id' : JobID,
				}
			).then (
				res=> {	
					
					if (res.data == "SUCCESSFULLY APPLIED") {
							alert("Successfully Applied");
					}else {
							alert ("Have a problem Can't apply");
					}
				}
			);
	}
	useEffect (()=>{
		
		if (sessionStorage.getItem('user_api_token')==null) {
			setHasToken(false);
		}else {
			axios.get('http://localhost/api/v2/list_job',
			).then (
				res=> {	
						let item_list = [];
						for(let i = 0;i < res.data.length;i++){
							item_list[i] = [ res.data[i].JobID, res.data[i].JobDescription, res.data[i].NameCompany, res.data[i].NumberSalary,res.data[i].EducationalBackground, res.data[i].ThePlace ]; 
						}
						setEventList(item_list);
				}
			);	
		}
	}, []);
	return (
		<div>
			<h2> Apply for a job </h2>
			<h3>Click 'Apply' for Apply job</h3>
			{!hasToken && <Redirect to="/" /> }
			{eventList.map(eventItem => (
				<h4> 
					{eventItem[0]} .Desscription: {eventItem[1]} 
					<br></br>
					Company : {eventItem[2]}  
					<br></br>
					Salary : {eventItem[3]}
					<br></br>
					Education : {eventItem[4]} 
					<br></br>
					Place : {eventItem[5]}   
					<br></br>
					<br></br>

					<button className = "btr" onClick={()=>{  sendSignup(eventItem[0]) } } >Apply</button> 
				
				</h4>
			))}
			
			
			
			
			
			
		</div>
	
	);
}

export default RegisterEvent;