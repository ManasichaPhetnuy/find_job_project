import {
    Link,
    Redirect
  } from "react-router-dom";
  
import './paint_login.css';

import logo from './dlogo.png';

  import { useState, useEffect } from 'react';
  import axios from 'axios';
  
  
  function Login() {
	
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [isLoggedInToMember, setIsLoggedInToMember] = useState(false);
	const [isLoggedInToAdmin,  setIsLoggedInToAdmin] =  useState(false);
	
	
	useEffect (()=>{
		//UseEffect จะทำงานเมื่อ Component ถูกโหลดมาตอนครั้งแรก
		//เราจะใช้ Check ว่า User Login ไว้หรือยัง  ถ้ายัง ให้ Login แต่ถ้า Login แล้ว ให้วิ่งไปหน้า Member Area เลย
		//alert(sessionStorage.getItem('api_token')==null);
		
		if (sessionStorage.getItem('user_api_token')!=null) {
			setIsLoggedInToMember(false);
		}
		
		if (sessionStorage.getItem('admin_api_token')!=null) {
			setIsLoggedInToAdmin(false);
		}
	
	}, []);
	
	function sendLogin(){	
	
			axios.post('http://localhost/api/v2/login',
				{
					"username" : username,
					"password" : password,
				}
			).then (
				res=> {				
					if (res.data.status=="success"){
						//SessionStorage เป็นคำสั่งมาตารฐานของ Javascript สำหรับการเก็บข้อมูลใดๆที่ใช้ชั่วคราว กรณีนี้เราใช้ Session Storage เก็บ api_token					
						if ( res.data.Admin == 'Not an Admin'){
							setIsLoggedInToMember(true);
							sessionStorage.setItem('user_api_token', res.data.token);
						}else{
							setIsLoggedInToAdmin(true);
							sessionStorage.setItem('admin_api_token', res.data.token);
						}
					}else {
						alert ("Login Failed");
					}
				}
			);	
	}
      
      return (
       <div>
        <div>
          <img src={logo} alt="cur" class="centers"
            height={100}
            width={150}
          />
        </div>
                  <br/><h3>Login</h3>
			
			<div className="form-group">
            <label>Username</label><br/>
			<input type="text" className="form-control" placeholder="Enter username" onChange={(e)=>{setUsername(e.target.value)}}/ >
            </div>          
            <div className="form-group">
            <label>Password</label><br/>
			<input type="password" className="form-control" placeholder="Enter password" onChange={(e)=>{setPassword(e.target.value)}}/ >
                        
            </div>
			
            <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" id="customCheck1" />
                            <label className="custom-control-label1" htmlFor="customCheck1">Remember me</label>
                        </div>
                    </div>
                    
                    <button type="submit" className="btn btn-primary btn-block" onClick={()=>{sendLogin()}}>Submit</button>
                    <div className="new-user text-right1">
                    {isLoggedInToMember && <Redirect to="/MemberArea" /> }
                    {isLoggedInToAdmin &&  <Redirect to="/AdminHome" /> }
                        New User? <Link to="/Regis">SignUp</Link>
                  
                    </div>
            </div>
       
            );
          }
          
    
            
    
    export default Login;