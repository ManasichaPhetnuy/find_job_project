<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Firebase\JWT\JWT;


$router->get('/', function () use ($router) {
    return $router->app->version();
});



//All job
$router->get('/list_job' , function () {
    
    $results = app('db')->select("SELECT * FROM JobInformation");
    
    return response()->json($results);

});



//Admin add job
$router->post('/admin_addjob', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select("SELECT Admin FROM JobRegister WHERE UserID = ? ",
								  [$user_id]);
	
	if ($results[0]->Admin == 'Not an Admin') {

		return "Permission Denied";
	}
    else {
		
	$job_description = $request->input("job_description");

    $name_company = $request->input("name_company");

    $number_salary = $request->input("number_salary");

    $educational_background = $request->input("educational_background");

    $the_place = $request->input("the_place");

    $query = app('db')->insert('INSERT into jobinformation
                            (JobDescription, NameCompany, NumberSalary, EducationalBackground, ThePlace)
                                       VALUES (?, ?, ?, ?, ?) ',
                                        [ $job_description,
                                          $name_company,
                                          $number_salary,
                                          $educational_background,
                                          $the_place ] );

    if($query)
     return response()->json(["STATUS"=>"SUCCESS"]);
    else
     return response()->json(["STATUS"=>"FAIL"]);                                      
                                        

	}

}]);




//Admin update job
$router->put('/admin_updatejob', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select("SELECT Admin FROM JobRegister WHERE UserID = ? ",
								  [$user_id]);
	
	if ($results[0]->Admin == 'Not an Admin') {
		return "Permission Denied";
	}
    else {
		
	$job_id = $request->input("job_id");

    $job_description = $request->input("job_description");

    $name_company = $request->input("name_company");

    $number_salary = $request->input("number_salary");

    $educational_background = $request->input("educational_background");

    $the_place = $request->input("the_place");

    $query = app('db')->update('UPDATE JobInformation
                                    SET JobDescription = ?,
                                        NameCompany = ?,
                                        NumberSalary = ?,
                                        EducationalBackground = ?,
                                        ThePlace = ?
                                     
                                    WHERE
                                        JobID = ?',
                                        [ $job_description,
                                          $name_company,
                                          $number_salary,
                                          $educational_background,
                                          $the_place,
                                          $job_id ] );

    return "UPDATE SUCCESSFULLY";

	}

}]);



//Admin delete job
$router->delete('/admin_deletejob', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select("SELECT Admin FROM JobRegister WHERE UserID = ? ",
								  [$user_id]);
	
	if ($results[0]->Admin == 'Not an Admin') {
		return "Permission Denied";
	}
    else {
		
	$job_id = $request->input("job_id");

    $query = app('db')->delete('DELETE FROM JobInformation
                                         WHERE
                                            JobID = ?',
                                        [ $job_id ] );

    return "DELETE SUCCESSFULLY";
    
	}

}]);



//User


//User and Admin login
$router->post('/login', function (Illuminate\Http\Request $request) {
    
    $username = $request->input("username");
    $password = $request->input("password");

    $result = app('db')->select('SELECT UserID, password, Admin FROM JobRegister WHERE Username = ?',
                            [$username]);

    $loginResult = new stdClass();

    if (count($result) == 0) {
        $loginResult->status = "fail";
        $loginResult->reason = "User is not founded";
    }
    else {
        if (app('hash')->check($password, $result[0]->password)) {
            $loginResult->status = "success";
            
            $payload = [
                'iss' => "findjob_system",
                'sub' => $result[0]->UserID,
                'iat' => time(), 
                'exp' => time() + 30 * 60 * 60, 
            ];

            $loginResult->token = JWT::encode($payload, env('APP_KEY'));
			$loginResult->Admin = $result[0]->Admin;

        }
        else {
            $loginResult->status = "fail";
            $loginResult->reason = "Incorrect Password";
            
        }
    }

    return response()->json($loginResult);
});



//Register User
$router->post('/register_user', function(Illuminate\Http\Request $request) {
    
    $name = $request->input("name");
    $surname = $request->input("surname");
    $age = $request->input("age");
    $birht_day = $request->input("birth_day");

    $educationalbackground = $request->input("educationalbackground");
    $finish_faculty = $request->input("finish_faculty");
    $finish_branch = $request->input("finish_branch");
    $graduate_university = $request->input("graduate_university");

    $gmail = $request->input("gmail");
    $phone_number = $request->input("phone_number");

    $username = $request->input("username");
    $password = app('hash')->make($request->input("password"));

  
    $query = app('db')->insert('INSERT into JobRegister
        (Name, Surname, Age, BirhtDay, Educational_Background, FinishFaculty,
        FinishBranch, GraduateUniversity, Gmail, PhoneNumber, Username, Password, Admin)
                                VALUES (?, ?, ?, ?, ?, ?, ?,
                                        ?, ?, ?, ?, ?, ?)',

                                [ $name,
                                  $surname,
                                  $age,
                                  $birht_day,
                                  $educationalbackground,
                                  $finish_faculty,
                                  $finish_branch,
                                  $graduate_university,
                                  $gmail,
                                  $phone_number,
                                  $username,
                                  $password,
                                  'Not an Admin' ] );
    if($query)
        return response()->json(["STATUS"=>"SUCCESS"]);
    else
    return response()->json(["STATUS"=>"FAIL"]);


});



//Profile User
$router->get('/user_profile', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select('SELECT * FROM JobRegister
							      WHERE (jobregister.UserID = ? )',
										[$user_id]);

	return response()->json($results);

}]);


//User registered to apply for a job
$router->post('/register_job', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$job_id = $request->input("job_id");
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$query = app('db')->insert('INSERT into JobConfirm
					(JobID, UserID, ConfirmJob)
					    VALUE (?, ?, ?)',
					    [ $job_id,
					      $user_id,
					     'Waiting for Confirmation'] );
	
    return "SUCCESSFULLY APPLIED";

}]);



//User views job applications
$router->get('/listjob_user', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select('SELECT JobDescription, NameCompany, NumberSalary, EducationalBackground, ThePlace, ConfirmJob
								  FROM JobInformation, JobConfirm, JobRegister
							      WHERE (JobConfirm.JobID = jobinformation.JobID )
								  AND (jobregister.UserID = ?)
                                  AND  (JobRegister.UserID = JobConfirm.UserID)',
										[$user_id]);

	return response()->json($results);

}]);



//Admin

//Admin ConFirm Job
$router->put('/admin_confirmjob', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select("SELECT Admin FROM JobRegister WHERE UserID = ? ",
								  [$user_id]);
	
	if ($results[0]->Admin == 'Not an Admin') {

		return "Permission Denied";
	}
    
    else {

		$id = $request->input("id");

		$query = app('db')->update('UPDATE JobConfirm
						SET ConfirmJob = ?
						WHERE ID = ?',
						[ 'PASS',
                            $id] );
		
        return "CONFIRMED";

	}

}]);



//Admin View User information
$router->get('/admin_listregister', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select("SELECT Admin FROM JobRegister WHERE UserID = ? ",
								  [$user_id]);
	
	if ($results[0]->Admin == 'Not an Admin') {
		return "Permission Denied";
	}
    else {
		
			$results = app('db')->select('SELECT ID, Name, Surname, JobDescription, NameCompany, ConfirmJob
												FROM JobInformation, JobConfirm, JobRegister
                                                WHERE (JobConfirm.JobID = jobinformation.JobID )
                                                AND (JobConfirm.ConfirmJob = ?)
                                                AND  (JobRegister.UserID = JobConfirm.UserID)',
												['Waiting for Confirmation'] ) ;
		
		return response()->json($results);
	}
	
}]);
