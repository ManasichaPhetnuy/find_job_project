-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for findjob_system
CREATE DATABASE IF NOT EXISTS `findjob_system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `findjob_system`;

-- Dumping structure for table findjob_system.jobconfirm
CREATE TABLE IF NOT EXISTS `jobconfirm` (
  `ID` int(100) NOT NULL AUTO_INCREMENT,
  `JobID` int(100) DEFAULT '0',
  `UserID` int(100) DEFAULT '0',
  `ConfirmJob` char(100) DEFAULT 'n',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table findjob_system.jobconfirm: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobconfirm` DISABLE KEYS */;
REPLACE INTO `jobconfirm` (`ID`, `JobID`, `UserID`, `ConfirmJob`) VALUES
	(1, 2, 5, 'Waiting for confirmation'),
	(2, 1, 5, 'PASS');
/*!40000 ALTER TABLE `jobconfirm` ENABLE KEYS */;

-- Dumping structure for table findjob_system.jobinformation
CREATE TABLE IF NOT EXISTS `jobinformation` (
  `JobID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `JobDescription` varchar(100) NOT NULL DEFAULT '',
  `NameCompany` varchar(100) NOT NULL DEFAULT '',
  `NumberSalary` varchar(50) NOT NULL,
  `EducationalBackground` varchar(100) NOT NULL DEFAULT '',
  `ThePlace` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`JobID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table findjob_system.jobinformation: ~3 rows (approximately)
/*!40000 ALTER TABLE `jobinformation` DISABLE KEYS */;
REPLACE INTO `jobinformation` (`JobID`, `JobDescription`, `NameCompany`, `NumberSalary`, `EducationalBackground`, `ThePlace`) VALUES
	(1, 'Programmer Developer', 'บริษัท เลิศวณิชย์', '20000-70000', 'ปริญญาตรี', 'กทม.'),
	(2, 'Junior web Application', 'บริษัท พรีไซซ คอร์ปอเรชั่น', 'สามารถเจรจาต่อรองได้', 'ปริญญาตรี', 'นนทบุรี'),
	(3, 'Programmer', 'บริษัท ตั้งใจ จำกัด', '50000-90000', 'ปริญญาตรี', 'สงขลา'),
	(4, 'Web Programmer', 'บริษัท บ้านพรุ จำกัด', '50000 ขึ้นไป', 'ปริญญาตรี', 'บ้านพรุ');
/*!40000 ALTER TABLE `jobinformation` ENABLE KEYS */;

-- Dumping structure for table findjob_system.jobregister
CREATE TABLE IF NOT EXISTS `jobregister` (
  `UserID` int(100) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL DEFAULT '',
  `Surname` varchar(100) NOT NULL DEFAULT '',
  `Age` char(100) NOT NULL DEFAULT '',
  `BirhtDay` date DEFAULT NULL,
  `Educational_Background` varchar(100) NOT NULL DEFAULT '',
  `FinishFaculty` varchar(100) NOT NULL DEFAULT '',
  `FinishBranch` varchar(100) NOT NULL DEFAULT '',
  `GraduateUniversity` varchar(100) NOT NULL DEFAULT '',
  `Gmail` varchar(100) NOT NULL,
  `PhoneNumber` varchar(100) NOT NULL DEFAULT '',
  `Username` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Admin` char(50) DEFAULT 'n',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table findjob_system.jobregister: ~2 rows (approximately)
/*!40000 ALTER TABLE `jobregister` DISABLE KEYS */;
REPLACE INTO `jobregister` (`UserID`, `Name`, `Surname`, `Age`, `BirhtDay`, `Educational_Background`, `FinishFaculty`, `FinishBranch`, `GraduateUniversity`, `Gmail`, `PhoneNumber`, `Username`, `Password`, `Admin`) VALUES
	(5, 'jj', 'jao', '23', '1998-10-09', 'ปริญญาเอก', 'วจก', 'บริหาร', 'สงขลานครินทร์', 'jj@psu.ac.th', '-', 'jj', '$2y$10$Uv38FM7Sn9y5eEhGcaNy9.Zmp/x6EpnJ/Xu34gAZlkHCc3GwdrkC6', 'Not an Admin'),
	(6, 'Sutaphat', 'Thongnui', '20', '2000-12-23', 'ปริญญาตรี', 'วิทย์', 'วิทยาคอม', 'สงขลานครินทร์', '6210210167@psu.ac.th', '0945961886', 'Admin', '$2y$10$8asQLn8OW4b.xrQJR91NguX7HSDmqBK.FiA2ed8eOn/qjYMM0emOu', 'Admin');
/*!40000 ALTER TABLE `jobregister` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
